require('../css/app.scss');
require('../img/logo.png');

var $ = require('jquery');
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap');
require('slick-carousel/slick/slick');

require('masonry-layout/masonry');
require('./main');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
    setup_event_slider();
});

var $collectionHolder;

// setup an "add a tag" link
var $addImageLink = $('<a href="#" class="add_image btn btn-info btn-sm"><i class="fa fa-plus mr-2" aria-hidden="true"></i> Voeg afbeelding toe</a>');
var $newLinkLi = $('<li></li>').append($addImageLink);

$(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionHolder = $('ul.images');

    // add a delete link to all of the existing tag form li elements
    $collectionHolder.find('li').each(function() {
        addImageFormDeleteLink($(this));
    });

    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addImageLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addImageForm($collectionHolder, $newLinkLi);
    });
});

function addImageForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li class="mb-3"></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}

function addImageFormDeleteLink($imageFormLi) {
    var $removeFormA = $('<a href="javascript:;">delete this image</a>');
    $imageFormLi.append($removeFormA);

    $removeFormA.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // remove the li for the tag form
        $imageFormLi.remove();
    });
}

function setup_event_slider() {
    $('.event-slider').slick({
        infinite: false,
        prevArrow: '',
        nextArrow: '',
        dots: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            ]
    });
}