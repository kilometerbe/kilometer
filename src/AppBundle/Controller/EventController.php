<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Event;
use AppBundle\Form\EventType;

class EventController extends Controller
{
    /**
     * @Route("/evenementen", name="eventlist")
     */
    public function listAction(Request $request)
    {
        $events = $this->getDoctrine()
            ->getRepository(Event::class)
            ->findAll();

        return $this->render('default/event/list.html.twig', [
            'events' => $events,
            'eventActive' => true
        ]);
    }

    /**
     * @Route("/evenementen/{event}", name="eventdetail", requirements={"event" = "\d+"})
     */
    public function detailAction(Request $request, Event $event)
    {
        setlocale(LC_ALL, 'nl_NL');
        
        return $this->render('default/event/detail.html.twig', [
            'event' => $event,
            'eventActive' => true
        ]);
    }

    /**
     * @Route("/admin/events", name="admineventlist")
     */
    public function adminlistAction(Request $request)
    {
        $events = $this->getDoctrine()
            ->getRepository(Event::class)
            ->findAll();

        return $this->render('admin/event/list.html.twig', [
            'events' => $events,
            'eventPageActive' => true
        ]);
    }

    /**
     * @Route("/admin/event/create", name="eventcreate")
     */
    public function createAction(Request $request)
    {
        // create a task and give it some dummy data for this example
        $event = new Event();

        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$athlete` variable has also been updated
            $event = $form->getData();

            // $file stores the uploaded file
            $image = $event->getImage();
            $file = $image->getPicture();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where images are stored
            $file->move(
                $this->getParameter('uploaded_images'),
                $fileName
            );


            $image->setPicture($fileName);

            // Update the 'brochure' property to store the file name
            // instead of its contents
            $event->setImage($image);

            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->persist($event);
            $em->flush();

            $this->addFlash(
                'success',
                'Event successfully saved!'
            );

            return $this->redirectToRoute('eventlist');
        }

        return $this->render('admin/event/create.html.twig', array(
            'form' => $form->createView(),
            'eventPageActive' => true
        ));
    }

    /**
     * @Route("/admin/event/edit/{event}", name="eventedit")
     */
    public function editAction(Request $request, Event $event)
    {
        $originalImage = $event->getImage();
        if (!empty($originalImage)) {
            $originalImage = $originalImage->getPicture();
        }

        $form = $this->createForm(EventType::class, $event, array('method' => 'PATCH'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            // $file stores the uploaded file
            $image = $event->getImage();

            if (!empty($image)) {
                $file = $image->getPicture();

                if (!empty($file)) {
                    // Generate a unique name for the file before saving it
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                    // Move the file to the directory where images are stored
                    $file->move(
                        $this->getParameter('uploaded_images'),
                        $fileName
                    );

                    $image->setPicture($fileName);

                    // Update the 'brochure' property to store the file name
                    // instead of its contents
                    $event->setImage($image);

                    $em->persist($image);
                } else {
                    $image->setPicture($originalImage);
                    $event->setImage($image);
                    $em->persist($image);
                }
            } else {
                $image->setPicture($originalImage);
                $event->setImage($image);
                $em->persist($image);
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Event successfully updated!'
            );

            return $this->redirectToRoute('admineventlist');
        }

        return $this->render('admin/event/edit.html.twig', [
            'form' => $form->createView(),
            'eventPageActive' => true
        ]);
    }
}
