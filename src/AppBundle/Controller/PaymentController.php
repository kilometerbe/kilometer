<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Payment;
use AppBundle\Form\PaymentType;
use Mollie_API_Client;

class PaymentController extends Controller
{
    /**
     * @Route("/schenking", name="payment_create")
     */
    public function createPayment(Request $request)
    {
        $payment = new Payment();

        $form = $this->createForm(PaymentType::class, $payment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $payment->setCreated(new \DateTime());
            $payment->setExecuted(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($payment);
            $em->flush();

            $key = $this->container->getParameter('mollie_key');

            $mollie = new Mollie_API_Client;
            $mollie->setApiKey($key);

            try {
                $molliePayment = $mollie->payments->create(array(
                    "amount"      => $payment->getAmount(),
                    "description" => $payment->getDescription(),
                    "redirectUrl" => "http://localhost:8000/schenking/" . $payment->getId(),
                    "webhookUrl"  => "https://webshop.example.org/mollie-webhook/",
                    "include"     => "details.qrCode"
                ));

                $payment->setMollieID($molliePayment->id);
                $em->flush();

                header("Location: " . $molliePayment->getPaymentUrl(), true, 303);
                exit;
            }
            catch (Mollie_API_Exception $e)
            {
                echo "API call failed: " . htmlspecialchars($e->getMessage());
                echo " on field " . htmlspecialchars($e->getField());
                exit;
            }

            $molliePayment = $mollie->payments->get($molliePayment->id);

            if ($molliePayment->isPaid())
            {
                $this->addFlash(
                    'success',
                    'Uw schenking is goed ontvangen, bedankt!'
                );
            }

            $this->addFlash(
                'danger',
                'Error'
            );

            //return $this->redirectToRoute('payment_success');
        }

        return $this->render('default/payment/create.html.twig', array(
            'form' => $form->createView(),
            'paymentActive' => true
        ));
    }

    /**
     * @Route("/schenking-geslaagd", name="payment_success")
     */
    public function paymentSuccess(Request $request)
    {
        return $this->render('default/payment/success.html.twig', array(
            'paymentActive' => true
        ));
    }

    /**
     * @Route("/schenking/{id}", name="payment_result")
     */
    public function paymentResult(Request $request, Payment $payment)
    {
        $key = $this->container->getParameter('mollie_key');

        $mollie = new Mollie_API_Client;
        $mollie->setApiKey($key);

        $molliePayment = $mollie->payments->get($payment->getMollieID());

        return $this->render('default/payment/result.html.twig', array(
            'paymentActive' => true,
            'paymentSuccess' => $molliePayment->isPaid(),
            'molliePayment' => $molliePayment
        ));
    }

    /**
     * @Route("/payment-status", name="payment_status")
     */
    public function updatePayment(Request $request)
    {
        $id = $request->get('id');

        $key = $this->container->getParameter('mollie_key');

        $mollie = new Mollie_API_Client;
        $mollie->setApiKey($key);

        $molliePayment = $mollie->payments->get($id);

        $payment = $this->getDoctrine()
            ->getRepository(Payment::class)
            ->find($id);

        if (!empty($payment)) {
            $em = $this->getDoctrine()->getManager();
            $payment->setStatus($molliePayment->getStatus());
            $em->flush();
        }

    }
}
