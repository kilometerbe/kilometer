<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Event;

class AdminController extends Controller
{
    /**
     * @Route("/admin/dashboard", name="admindashboard")
     */
    public function adminDashboardAction(Request $request)
    {
        $upcomingEvents = $this->getDoctrine()
            ->getRepository(Event::class)
            ->findByDateInFuture();

        return $this->render('admin/dashboard.html.twig', [
            'upcomingEvents' => $upcomingEvents
        ]);
    }
}
