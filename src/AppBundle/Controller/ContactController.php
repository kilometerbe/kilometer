<?php
/**
 * Created by PhpStorm.
 * User: exsited
 * Date: 19/02/2018
 * Time: 19:52
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ContactType;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm(ContactType::class, null);

        $form->handleRequest($request);
        $success = false;

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->sendEmail($form->getData())) {
                $success = true;
            } else {
                // An error ocurred, handle
                var_dump("Errooooor :(");
            }
        }

        return $this->render('default/contact/contact.html.twig', array(
            'form' => $form->createView(),
            'success' => $success,
            'contactActive' => true
        ));
    }

    private function sendEmail($data)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Kilometer.be contactaanvraag')
            ->setFrom($this->getParameter('mailer_user'))
            ->setTo($data['email'])
            ->setBody(
                $this->renderView(
                    'emails/contact.html.twig',
                    array(
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'subject' => $data['subject'],
                        'message' => $data['message']
                    )
                ),
                'text/html'
            );

        return $this->get('mailer')->send($message);
    }
}