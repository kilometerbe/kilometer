<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Payment;
use AppBundle\Form\PaymentType;

class AdminPaymentController extends Controller
{
    /**
     * @Route("/admin/payments", name="adminpaymentlist")
     */
    public function listAction(Request $request)
    {
        $payments = $this->getDoctrine()
            ->getRepository(Payment::class)
            ->findAll();

        return $this->render('admin/payment/list.html.twig', [
            'payments' => $payments,
            'paymentPageActive' => true
        ]);
    }

    /**
     * @Route("/admin/payment/create", name="paymentcreate")
     */
    public function createAction(Request $request)
    {
        $payment = new Payment();

        $form = $this->createForm(PaymentType::class, $payment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $payment->setCreated(new \DateTime());
            $payment->setExecuted(true);

            $em = $this->getDoctrine()->getManager();
            $payment->setStatus(Payment::STATUS_PAID);
            $em->persist($payment);
            $em->flush();

            $this->addFlash(
                'success',
                'De betaling is toegevoegd!'
            );

            return $this->redirectToRoute('adminpaymentlist');
        }

        return $this->render('admin/payment/create.html.twig', [
            'form' => $form->createView(),
            'paymentPageActive' => true
        ]);
    }
}