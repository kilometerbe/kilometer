<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Athlete;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $athletes = $this->getDoctrine()
            ->getRepository(Athlete::class)
            ->findAll();

        return $this->render('default/home.html.twig', [
            'athletes' => $athletes,
            'homeActive' => true
        ]);
    }
}
