<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Athlete;
use AppBundle\Form\AthleteType;

use Cocur\Slugify\Slugify;

class AthleteController extends Controller
{
    /**
     * @Route("/admin/athletes", name="adminathletelist")
     */
    public function athleteListAction(Request $request)
    {
        $athletes = $this->getDoctrine()
            ->getRepository(Athlete::class)
            ->findAll();

        return $this->render('admin/athlete/list.html.twig', [
            'athletes' => $athletes,
            'athletePageActive' => true
        ]);
    }

    /**
     * @Route("/admin/athlete/{athlete}", name="athletedetail", requirements={"athlete" = "\d+"})
     */
    public function detailAction(Request $request, Athlete $athlete)
    {
        $athlete->getEvents()->initialize();

        return $this->render('admin/athlete/detail.html.twig', [
            'athlete' => $athlete,
            'athletePageActive' => true
        ]);
    }

    /**
     * @Route("/admin/athlete/create", name="athletecreate")
     */
    public function createAction(Request $request)
    {
        // create a task and give it some dummy data for this example
        $athlete = new Athlete();

        $form = $this->createForm(AthleteType::class, $athlete);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $images = $form['images']->getData();
            $em = $this->getDoctrine()->getManager();
            foreach ($images as $image) {
                $file = $image->getPicture();

                // Generate a unique name for the file before saving it
                $fileName = md5(uniqid()).'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('uploaded_images'),
                    $fileName
                );

                $slugify = new Slugify();
                $slug =  $slugify->slugify($athlete->getFirstname()); // hello-world
                $athlete->setSlug($slug);

                $image->setPicture($fileName);
                $em->persist($image);
            }

            // $file stores the uploaded file
            $coverImage = $athlete->getCover();
            $file = $coverImage->getPicture();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            // Move the file to the directory where images are stored
            $file->move(
                $this->getParameter('uploaded_images'),
                $fileName
            );

            $coverImage->setPicture($fileName);

            // Update the 'brochure' property to store the file name
            // instead of its contents
            $athlete->setCover($coverImage);

            $em = $this->getDoctrine()->getManager();
            $em->persist($coverImage);
            $em->persist($athlete);
            $em->flush();

            $this->addFlash(
                'success',
                'Athlete successfully saved!'
            );

            return $this->redirectToRoute('adminathletelist');
        }

        return $this->render('admin/athlete/create.html.twig', array(
            'form' => $form->createView(),
            'athletePageActive' => true
        ));
    }

    /**
     * @Route("/admin/athlete/edit/{athlete}", name="athleteedit")
     */
    public function editAction(Request $request, Athlete $athlete)
    {
        $originalCover = $athlete->getCover();
        if (!empty($originalCover)) {
            $originalCover = $originalCover->getPicture();
        }

        $originalImages = array();
        foreach ($athlete->getImages() as $image) {
            $originalImages[$image->getId()] = $image->getPicture();
        }

        $originalImageCollection = new ArrayCollection();
        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($athlete->getImages() as $image) {
            $originalImageCollection->add($image);
        }

        $form = $this->createForm(AthleteType::class, $athlete, array('method' => 'PATCH'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $images = $athlete->getImages();

            // remove the relationship between the image and the room
            foreach ($originalImageCollection as $image) {

                if (false === $images->contains($image)) {
                    // remove the Task from the Tag
                    $athlete->getImages()->removeElement($image);
                    $em->persist($image);
                }
            }

            foreach ($images as $image) {
                $file = $image->getPicture();

                if ($file) {
                    // Generate a unique name for the file before saving it
                    $fileName = md5(uniqid()).'.'.$file->guessExtension();

                    // Move the file to the directory where brochures are stored
                    $file->move(
                        $this->getParameter('uploaded_images'),
                        $fileName
                    );

                    $image->setPicture($fileName);
                }
                else {
                    $image->setPicture($originalImages[$image->getId()]);
                }

                $em->persist($image);
            }

            // $file stores the uploaded file
            $coverImage = $athlete->getCover();

            if (!empty($coverImage)) {
                $file = $coverImage->getPicture();

                if (!empty($file)) {
                    // Generate a unique name for the file before saving it
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                    // Move the file to the directory where images are stored
                    $file->move(
                        $this->getParameter('uploaded_images'),
                        $fileName
                    );

                    $coverImage->setPicture($fileName);

                    // Update the 'brochure' property to store the file name
                    // instead of its contents
                    $athlete->setCover($coverImage);

                    $em->persist($coverImage);
                } else {
                    $coverImage->setPicture($originalCover);
                    $athlete->setCover($coverImage);
                    $em->persist($coverImage);
                }
            } else {
                $coverImage->setPicture($originalCover);
                $athlete->setCover($coverImage);
                $em->persist($coverImage);
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Athlete successfully updated!'
            );

            return $this->redirectToRoute('adminathletelist');
        }

        return $this->render('admin/athlete/edit.html.twig', [
            'form' => $form->createView(),
            'athletePageActive' => true
        ]);
    }

    /**
     * @Route("/atleten/{slug}", name="athlete_detail_public")
     */
    public function detailPublicAction(Request $request, $slug)
    {
        $athlete = $this->getDoctrine()
            ->getRepository(Athlete::class)
            ->findOneBySlug($slug);

        return $this->render('default/athlete/detail.html.twig', [
            'athlete' => $athlete,
            'athletePageActive' => true
        ]);
    }
}
