<?php
/**
 * Created by PhpStorm.
 * User: exsited
 * Date: 19/01/2018
 * Time: 16:16
 */

namespace AppBundle\Form;

use AppBundle\Entity\Payment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('creator', TextType::class, array(
                'label' => 'payment.creator'
            ))
            ->add('email', EmailType::class, array(
                'label' => 'payment.email',
                'required' => false
            ))
            ->add('amount', NumberType::class, array(
                'label' => 'payment.amount',
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'payment.description',
                'required' => false
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'payment.donate'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Payment::class,
        ));
    }
}