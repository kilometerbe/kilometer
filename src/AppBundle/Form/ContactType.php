<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('attr' => array('placeholder' => 'Uw naam'),
                'constraints' => array(
                    new NotBlank(array("message" => "Gelieve uw naam op te geven")),
                )
            ))
            ->add('subject', TextType::class, array('attr' => array('placeholder' => 'Onderwerp'),
                'constraints' => array(
                    new NotBlank(array("message" => "Gelieve een onderwerp op te geven")),
                )
            ))
            ->add('email', EmailType::class, array('attr' => array('placeholder' => 'Uw email-adres'),
                'constraints' => array(
                    new NotBlank(array("message" => "Gelieve uw email-adres op te geven")),
                    new Email(array("message" => "Het email-adres dat u opgaf is niet geldig")),
                )
            ))
            ->add('message', TextareaType::class, array('attr' => array('placeholder' => 'Uw bericht'),
                'constraints' => array(
                    new NotBlank(array("message" => "Gelieve uw bericht hier te schrijven")),
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'error_bubbling' => true
        ));
    }

    public function getName()
    {
        return 'contact_form';
    }
}