<?php
/**
 * Created by PhpStorm.
 * User: exsited
 * Date: 19/01/2018
 * Time: 16:16
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use AppBundle\Entity\Athlete;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityManagerInterface;

class EventType extends AbstractType
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextAreaType::class)
            ->add('date', DateType::class, array(
                'html5' => false,
                'placeholder' => array(
                    'year' => 'Jaar', 'month' => 'Maand', 'day' => 'Dag'
                ),
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'
            ))
            ->add('distance', NumberType::class)
            ->add('location', TextType::class)
            ->add('athletes', EntityType::class, array(
                // query choices from this entity
                'class' => 'AppBundle:Athlete',

                // use the User.username property as the visible option string
                'choice_label' => 'firstname',

                // used to render a select box, check boxes or radios
                'multiple' => true,
                // 'expanded' => true,
            ))
            ->add('image', ImageType::class, array(
                'label' => 'Afbeelding',
                'required' => false
            ))
            ->add('save', SubmitType::class, array('label' => 'Evenement opslaan'))
        ;
    }
}