<?php
/**
 * Created by PhpStorm.
 * User: exsited
 * Date: 19/01/2018
 * Time: 16:16
 */

namespace AppBundle\Form;

use AppBundle\Entity\Athlete;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AthleteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class,
                [
                    'label' => 'field.firstname'
                ])
            ->add('lastname', TextType::class,
                [
                    'label' => 'field.lastname'
                ])
            ->add('nickname', TextType::class,
                [
                    'label' => 'field.nickname'
                ])
            ->add('email', EmailType::class,
                [
                    'label' => 'field.email'
                ])
            ->add('city', TextType::class,
                [
                    'label' => 'field.city'
                ])
            ->add('birthdate', DateType::class, array(
                'label' => 'field.birthdate',
                'html5' => false,
                'placeholder' => array(
                    'year' => 'Jaar', 'month' => 'Maand', 'day' => 'Dag'
                ),
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'
            ))
            ->add('biography', TextareaType::class, array(
                'required' => false,
                'label' => 'field.biography',
            ))
            ->add('goals', TextareaType::class, array(
                'required' => false,
                'label' => 'field.goals',
            ))
            ->add('website', TextType::class, array(
                'required' => false,
                'label' => 'field.website',
            ))
            ->add('job', TextType::class, array(
                'required' => false,
                'label' => 'field.job',
            ))
            ->add('facebook', TextType::class, array(
                'required' => false,
                'label' => 'field.facebook',
            ))
            ->add('instagram', TextType::class, array(
                'required' => false,
                'label' => 'field.instagram',
            ))
            ->add('cover', ImageType::class, array(
                'label' => 'field.cover',
                'required' => false
            ))
            ->add('images', CollectionType::class, array(
                'label' => 'field.extraimages',
                'required' => false,
                'entry_type' => ImageType::class,
                'allow_add' => true,
                'allow_delete' => true
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Atleet opslaan'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Athlete::class,
        ));
    }
}